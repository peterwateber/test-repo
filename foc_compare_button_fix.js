/** ### VEHICLE COMPARE BUTTON UPGRADE ### **/

$('body').off('click', '.vehicle label.compareBtn input:checkbox');
$('.vehicle label.compareBtn input:checkbox').unbind('click');

var styleHeader = '<style>\
						.quickViewCompareWrap,\
						#comparisonModal #compare-vehicle-table .compareTableTitleWrapper .compareTableTitle {\
    						color: #000;\
						}\
					</style>';
$('head').append(styleHeader);

$('body').on('click', '.vehicle label.compareBtn input:checkbox', function(){
	var $inp = $(this);
	var vehicleId =  $inp.closest('div[itemprop="itemListElement"]').attr('data-vid');
  	var $compareSortable = $('div.sidenav div#imgSortable');
    if($inp.is(':checked')) {

      var vehicleAlreadyInCompare = $('div.imgCompareWrapper[data-vid = ' + vehicleId + ']');

      //Confirm user is not adding too many vehicles
      if (($compareSortable.find('div.imgCompareWrapper').length+1 > inventoryMenuCommon.attrs['data-num_vehicles_to_compare'])){
        var msg = inventoryMenuCompare.translations.toolTipMsg.replace('__MAXNUM__', inventoryMenuCommon.attrs['data-num_vehicles_to_compare']);
        $inp.attr('checked', false);

        if($inp.is('input')){
          $inp = $inp.closest('div');
        }
        bpUtilities.addToolTiptoElement($inp.find('label'), msg, false, true);
        return false;
      } else {
        inventoryMenuCompare.getVehicleByIdAndAddToCompSide(vehicleId);
        $inp.prop('disabled',true);
        
        var $img = $('<img/>').attr({
           'src' : 'https://s3-us-west-1.amazonaws.com/blueprint-cdn.searchoptics.com/6e9166d52cd1612d59069cf5093c997d/images/preloader.gif',
           'class' : 'comp-preloader',
          	'style': 'vertical-align: middle; padding-left: 5px;'
        });
        $inp.parent().append($img);
        
        $(document).ajaxComplete(function(){
          $inp.prop('disabled',false);
          $inp.parent().find('.comp-preloader').remove();
          inventoryMenuCompare.addVehicleToCompareAnimation($inp.closest('div'));
        });
      }

    } else {
      inventoryMenuCompare.removeVehicleFromCompare(vehicleId);
      $compareSortable.find('div.imgCompareWrapper[data-vid="' + vehicleId +'"]').remove();
      inventoryMenuCompare.toggleTriggerCompareBtn();
    }
    $compareSortable.sortable("refresh");
    return true;
  
});

/* ###### */